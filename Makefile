# https://www.gnu.org/software/make/manual/make.html#Makefile-Basics
SHELL = /bin/sh
INSTALL = install

prefix = /usr/local
datarootdir = $(prefix)/share
datadir = $(datarootdir)
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
sysconfdir = $(prefix)/etc

all: build/automatic-backup build/automatic-backup-recycle

build/automatic-backup: automatic-backup.sh
	[ -d ./build ] || mkdir ./build
	cp ./automatic-backup.sh ./build/automatic-backup
	chmod +x ./build/automatic-backup

build/automatic-backup-recycle: automatic-backup-recycle.sh
	[ -d ./build ] || mkdir ./build
	cp ./automatic-backup-recycle.sh ./build/automatic-backup-recycle
	chmod +x ./build/automatic-backup-recycle

install: build/automatic-backup build/automatic-backup-recycle
	$(INSTALL) -m 755 -D \
		./build/automatic-backup $(DESTDIR)$(bindir)/
	$(INSTALL) -m 755 -D \
		./build/automatic-backup-recycle $(DESTDIR)$(bindir)/

uninstall:
	rm -v \
		$(DESTDIR)$(bindir)/automatic-backup \
		$(DESTDIR)$(bindir)/automatic-backup-recycle \

clean:
	rm -v -r ./build

.PHONY: all install uninstall clean
