#!/bin/sh
# Erases old backups to free space

dry_run=false
disk_threshold=75
days_old=30
verbose=false
usage="Usage: automatic-backup-recycle [-D] [-v] [-t DISK_THRESHOLD] [-d DAYS] backup_dir"

args=`getopt Dvt:d: $*`
if [ $? -ne 0 ]; then
	echo "$usage" >&2
	exit 2
fi

eval set -- "$args"

while true
do
	case "$1"
	in
		-D)
			dry_run=true; shift;;
		-v)
			verbose=true; shift;;
		-t)
			disk_threshold="$2"; shift; shift;;
		-d)
			days_old="$2"; shift; shift;;
		--)
			shift; break;;
		*)
			echo "Error parsing options" >&2; exit 1;;
	esac
done

if [ "$1" = "" ]; then
	echo "Missing required argument: backup directory.\n" "$usage" >&2
	exit 2
fi

backup_dir="$1"

if [ ! -d "$backup_dir" ]; then
	echo 'Backup directory not found' >&2
	exit 1
fi

if [ $dry_run = true ]; then
	echo "[DRY RUN] Threshold ignored"
else
	[ $verbose = true ] && echo "Will free space when usage exceeds" "$disk_threshold"%
fi

disk_used=""

update_disk_used() {
	disk_used=$(df -kP "$backup_dir" | \
		tail -n 1 | \
		awk -F ' ' '{print $5}' | \
		grep -o '[0-9]\{1,\}')
}

update_disk_used
[ $verbose = true ] && echo "Disk used:" "$disk_used"%

if [ $dry_run = true ] || [ "$disk_used" -gt "$disk_threshold" ]; then
	echo "Threshold exceeded (${disk_used}%). Time to free some space."

	find_output=""

	if [ $dry_run = true ]; then
		find_output=$(find "$backup_dir" -mindepth 1 -maxdepth 1 -type d -mtime +"$days_old" -exec echo 'Removing' {} \;)
	else
		find_output=$(find "$backup_dir" -mindepth 1 -maxdepth 1 -type d -mtime +"$days_old" -exec rm -v -r {} \;)
	fi

	if [ "$find_output" = "" ]; then
		echo 'Nothing to remove'
	else
		echo "$find_output"
	fi

	if [ $dry_run = false ]; then
		update_disk_used
		echo "Disk used:" "$disk_used"%
	fi
else
	[ $verbose = true ] && echo 'Nothing to do'
fi

exit 0
