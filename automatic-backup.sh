#!/bin/sh
# Creates backup archives

usage='usage: automatic-backup output_dir'
now=$(date +"%Y-%m-%d_%H:%M:%S")

if [ -z "$1" ]; then
	echo 'syntax error: missing output_dir\n' "$usage" >&2
	exit 2
fi

output_dir="$1"

if [ ! -w "$output_dir" ] || [ ! -d "$output_dir" ]; then
	echo 'output_dir is not a writable and existing directory' >&2
	exit 1
fi

if [ ! -r /etc/automatic-backup ]; then
	echo 'configuration file missing or unreadable: /etc/automatic-backup' >&2
	exit 1
fi

# No read permission to others
umask 0007

mkdir "${output_dir}/${now}"

create_archive() {
	cd "$(dirname $1)"
	filename=$(basename "$1")
	tar -czf "${output_dir}/${now}/${2}.tar.gz" "./${filename}"
}

while read -r line; do
	label=$(echo "$line" | cut -f1)
	path=$(echo "$line" | cut -f2)

	if [ ! -r "$path" ]; then
		echo "Path does not exist or is unreadable: $path" >&2
		continue
	fi

	if [ "$label" = "$path" ]; then
		# only path was given, so use its basename as label
		label=$(basename "$path")
	fi

	create_archive "$path" "$label"
done < /etc/automatic-backup
