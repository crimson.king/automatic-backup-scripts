# automatic-backup-scripts

## Build and install
Run `make`, and then `make install`.

Default installation path is `/usr/local`

## Usage
Create the file `/etc/automatic-backup` and add a line for each path that you want archived.

```
/home/johnny
/home/jane/My share
/etc/wireguard/wg0.conf
/etc/fstab
```

Now test it:

```sh
automatic-backup /mnt/backup
```

e.g. `johnny.tar.gz`, `My share.tar.gz`, `wg0.tar.gz` and `fstab.tar.gz` will be created in `/mnt/backup/<timestamp>/`. The filenames come from `basename`.

You can also specify a name for each archive in the format `name \t path` (TAB-separated):
```
johnny-home-dir	/home/johnny
samba share	/home/jane/My share
wireguard-wg0	/etc/wireguard/wg0.conf
/etc/fstab
```
e.g. `johnny-home-dir.tar.gz`, `samba share.tar.gz`, `wireguard-wg0.tar.gz` and `fstab.tar.gz`.

## Managing disk space
Run `automatic-backup-recycle` periodically to delete old archives.

```
automatic-backup-recycle [-D] [-t PERCENTAGE] [-d DAYS] backup_directory
```

- `-t PERCENTAGE` lets you specify when to start erasing old archives. The argument should be a value between 0 and 100 — the percentage of disk used.
- `-d DAYS` lets you specify how many days old an archive should be, in order to be eligible for deletion. It refers to the modification time.
- `-D` is a "dry run".

```sh
automatic-backup-recycle -t 80 -d 30 /mnt/backup
```

i.e. When the disk is at least 80% full, it will delete folders older than 30 days.
